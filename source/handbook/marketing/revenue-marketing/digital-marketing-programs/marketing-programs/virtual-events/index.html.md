---
layout: markdown_page
title: "Virtual Events"
---

## Overview

This page focuses on the different types of Virtual Events run by the Marketing Programs team. 

## Who to work with

Virtual events that fall within the scope of an integrated campaign will be supported by the MPM DRI for that integrated campaign. Virtual events that fall outside the scope of an integrated campaign will be supported by @aoetama.

For a list of active integrated campaigns and MPM DRIs for each campaign, please reference the [Integrated Campaigns handbook page](/handbook/marketing/campaigns/). If you have any questions please post in the [#marketing_programs Slack channel](https://gitlab.slack.com/messages/CCWUCP4MS).

## Types of Virtual Events in GitLab

There are 6 different types of virtual events in GitLab.

* **Hosted demand gen webcast:** This is an internally hosted webcast geared towards generating net new leads or to nurture/educate existing prospects. Marketing programs normally own everything from the registration process, hosting the webcast on our webcast platform (zoom), all the way to post-event follow up.
* **Sponsored demand gen webcast:** This is webcast hosted on an external partner platform (e.g: DevOps.com) geared towards generating net new leads or to help push leads further down the sales funnel. We pay the partner a fee to host this webcast so they are responsible for driving registration, moderating and hosting the webcast on their platform. MOPs will be responsible for uploading the list to our database and MPMs will be responsible for sending post-event follow-up emails.
* **Account upsell webcast:**  This is an internally hosted webcast geared towards up-selling to an existing customer account. Marketing programs will work in collaboration with sales on the registration process. Marketing programs will also responsible for hosting the webcast on our webcast platform (zoom) and sending post-event follow-up emails.
* **Sponsored Virtual Conference:** This is not a webcast but rather a virtual conference where we pay a sponsorship fee to get a virtual booth and sometimes a speaking session slot. Marketing programs will primarily be responsible for sending the post-event follow-up emails.
* **Reseller enablement webcast:** This is an internally hosted webcast geared towards educating resellers on GitLab products/services as well as sales and marketing materials. Marketing programs will work in collaboration with the channel manager on the registration process. Marketing programs will also responsible for hosting the webcast on our webcast platform (zoom) and sending post-event follow-up emails.
* **Community webcast:** This is an internally hosted webcast driven by the community team to engage with GitLab user groups and community members. The community team is responsible for driving registration and sending post-event follow-up. The Marketing programs team supports the community team by hosting the webcast on our internal webcast platform (as needed).

## Below is a step by step guide on how to request Marketing Programs support for a virtual event.

**⚠ Note: Virtual Event requests should be submitted no less than 45 business days before the event date so the new request can be added into the responsible Marketing Program Manager's (MPM) workflow and to ensure we have enough leeway for promotion time.**

#### Step 1: Create a [virtual event request issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM_VirtualEvent_Request.md) in the digital marketing programs project.
*  Please use the `MPM_VirtualEvent_Request.md` issue template linked above
*  Please put the date of the virtual event as a due date
*  Please specify the type of virtual event
*  @ mention MPM in the issue comment to confirm the requested date is feasible
*  MPM will check the requested date against [the virtual events calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) to make sure there is no overlapping virtual event that has been pre-scheduled
*  If the requested date is feasible and the speakers have been secured the Marketing Programs team will change the status label from `status:plan` to `status:wip`, add the `Webcast` label and applicable `FY..` label (to make sure this appears on the [webcast issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/922606?&label_name[]=Webcast))

#### Step 2: MPM will create the Virtual Event EPIC.
*  When "status:wip" is on the issue and necessary elements are documented, MPM creates epic for the virtual event.
*  Naming convention: [Virtual Event Name] - [3-letter Month] [Date], [Year]
*  MPM copy/pastes code below into the epic

```
## [Main Issue >>]()

## [GANTT >>]()

## [Copy for landing page and emails >>]() - [template](https://docs.google.com/document/d/1j43mf7Lsq2AXoNwiygGAr_laiFzmokNCfMHi7KNLjuA/edit)

## Event Details
  * `place details from the event issue here`
  * [ ] [main salesforce campaign]()
  * [ ] [main marketo program]()

## Issue creation

* [ ] Secure presenters and dry runs issue created - MPM
* [ ] Only for Virtual sponsorship and 3rd Party Hosted webcast: List clean and upload issue created - MOps
* [ ] Landing page issue created - MPM
* [ ] Optional: New design assets issue created for the design team - MPM
* [ ] Invitation and reminder issue created - MPM
* [ ] Organic social issue created for social media manager - MPM
* [ ] Paid Ads issue created for DMP - MPM
* [ ] PathFactory request issue created - MPM
* [ ] Follow up email issue created - MPM
* [ ] On-demand switch issue created - MPM

cc @nlarue to create list upload issue (Only for Virtual sponsorship and 3rd Party Hosted webcast)

```

#### Step 3: MPM will create the necessary MPM support issues (linked in table below) and add to epic.

The table below shows execution tasks related to various types of Virtual Events along with the DRI for each task.

<div class="tg-wrap">
<table class="tg">
  <tr>
    <th class="tg-k6pi">Tasks</th>
    <th class="tg-k6pi">Due dates</th>
    <th class="tg-yw4l">Hosted demand gen webcast</th>
    <th class="tg-yw4l">Sponsored demand gen webcast</th>
    <th class="tg-yw4l">Sponsored Virtual Conference</th>
    <th class="tg-yw4l">Account specific webcast</th>
    <th class="tg-yw4l">Reseller enablement webcast</th>
    <th class="tg-yw4l">Community webcast</th>
  </tr>
  <tr>
    <td><strong> <a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/%20MPM-00-secure-presenters_dryruns.md">Secure presenters</a></strong></td>
    <td>-45 business days</td>
    <td>MPM or Requestor</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
    <td>Channel Sales Manager</td>
    <td>Community Program Manager</td>
  </tr>
  <tr>
    <td><strong> Create MPM Epic, issues, and fill out GANTT</strong></td>
    <td>-43 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM/td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM or Requestor</td>
  </tr>
  <tr>
    <td><strong> Host prep call with internal team</strong></td>
    <td>-40 business days</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>Only for large segment accounts: MPM</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
   <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-02-landing-page.md"> Landing page copy due </a></strong></td>
    <td>-35 business days</td>
    <td>Requestor or Presenter</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
    <td>Channel Sales Manager</td>
    <td>Community Program Manager</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/new?nav_source=navbar">New design assets delivered</a></strong></td>
    <td>-35 business days</td>
    <td>Design</td>
    <td>Design</td>
    <td>Design</td>
    <td>Design</td>
    <td>Design</td>
    <td>Design</td>
  </tr>
  <tr>
    <td><strong> <a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-02-landing-page.md"> Create landing page MR </a> </strong></td>
    <td>-33 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>Optional:MPM</td>
  </tr>
  <tr>
    <td><strong> Set up tracking in SFDC, Marketo, and Zoom </strong></td>
    <td>-33 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
   <tr>
    <td><strong> Drive registration - create issue for blog post promotion </strong></td>
    <td>-30 business days</td>
    <td>Optional: Requestor</td>
    <td>Only if we have paid ads budget: Requestor</td>
    <td>Only if we have paid ads budget: Requestor</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>Community program manager</td>
  </tr>
  <tr>
    <td><strong> Paid Ads LIVE </strong></td>
    <td>-30 business days</td>
    <td>DMP; MPM creates the Paid Ads request issue</td>
    <td>Only if we gave paid ads budget: DMP; Requestor creates the Paid Ads request issue</td>
    <td>Only if we gave paid ads budget: DMP; Requestor creates the Paid Ads request issue</td>
    <td>N/A</td>
    <td>Only if we gave paid ads budget: DMP; MPM creates the Paid Ads request issue </td>
    <td>Community program manager</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03B-virtualevent-promotion.md"> Promote in bi-weekly newsletter </a> </strong></td>
    <td>-21 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>Community program manager</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/corporate-marketing/blob/master/.gitlab/issue_templates/social-request.md"> Organic social promotion LIVE </a> </strong></td>
    <td>-21 business days</td>
    <td>Social; MPM creates organic social request issue</td>
    <td>Social; Requestor creates organic social request issue</td>
    <td>Social; Requestor creates organic social request issue</td>
    <td>Optional(to be discussed in prep call): Social; MPM creates organic social request issue</td>
    <td>N/A</td>
    <td>Social; Community program manager creates organic social request issue</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03-invitations-reminder.md"> Marketing email invite copy written </a> </strong></td>
    <td>-21 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>MPM</td>
    <td>N/A</td>
  </tr>
  <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-03-invitations-reminder.md"> Marketing email invite scheduled </a> </strong></td>
    <td>-16 business days</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>MPM</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong> Presentation slides due </strong></td>
    <td>-7 business days</td>
    <td>Requestor or Presenter</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>Optional (to be discusseed in prep call): SALs</td>
    <td>Channel Sales Manager</td>
    <td>Community program manager</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-04-follow-up-email.md"> Post event follow up email copy written </a></strong></td>
    <td>-5 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>SALs</td>
    <td>Channel Sales Manager</td>
    <td>Community program manager</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/%20MPM-00-secure-presenters_dryruns.md">  Host dry run </a> </strong></td>
    <td>-5 business days</td>
    <td>MPM</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong> Host virtual event</strong></td>
    <td>0 business days</td>
    <td>MPM</td>
    <td>3rd party vendor</td>
    <td>3rd party vendor</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong> Moderate webcast</strong></td>
    <td>0 business days</td>
    <td>MPM or Presenter</td>
    <td>3rd party vendor</td>
    <td>N/A</td>
    <td>SALs</td>
    <td>Channel Sales Manager</td>
    <td>Community program manager</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-06-on-demand-switch.md"> Upload live webcast recording to youtube</a></strong></td>
    <td>+1 business days</td>
    <td>MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (to be discussed in prep call): MPM</td>
    <td>MPM</td>
    <td>MPM</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/pathfactory_request.md"> Upload youtube recording to PathFactory</a></strong></td>
    <td>+1 business days</td>
    <td>DMP</td>
    <td>Optional (if available): DMP</td>
    <td>Optional (if available): DMP</td>
    <td>Optional (to be discussed in prep call): MPM</td>
    <td>DMP</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-06-on-demand-switch.md"> Convert registration page to on-demand </a> </strong></td>
    <td>+2 business days</td>
    <td>MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (if available): MPM</td>
    <td>Optional (to be discussed in prep call): MPM</td>
    <td>MPM</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/blob/master/.gitlab/issue_templates/event-clean-upload-list.md"> Forward cleaned lead list to MOPs for upload </a></strong></td>
    <td>+2 business days</td>
    <td>N/A</td>
    <td>Requestor</td>
    <td>Requestor</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/blob/master/.gitlab/issue_templates/event-clean-upload-list.md"> MOPs finished uploading list to SFDC (CRM) </a></strong></td>
    <td>+4 business days</td>
    <td>N/A</td>
    <td>MOPs</td>
    <td>MOPs</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
    <tr>
    <td><strong><a href="https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/blob/master/.gitlab/issue_templates/MPM-04-follow-up-email.md"> Send marketo follow up emails </a></strong></td>
    <td>+2 to +5 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM or Community program manager</td>
  </tr>
    <tr>
    <td><strong> Facilitate retrospective </strong></td>
    <td>+7 business days</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>MPM</td>
    <td>N/A</td>
    <td>N/A</td>
    <td>N/A</td>
  </tr>
  </table>
</div>

### Virtual events calendar

The calendar below documents all scheduled virtual events.

<figure>
<iframe src="https://calendar.google.com/calendar/b/1/embed?showPrint=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=gitlab.com_1qve6g81dp91r9hevtkfd9r098%40group.calendar.google.com&amp;color=%23B1365F&amp;ctz=America%2FLos_Angeles" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
<figure/>

