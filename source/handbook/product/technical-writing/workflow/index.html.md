---
layout: handbook-page-toc
title: "Technical Writing workflows"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This document explains the workflows of the Technical Writing team.

These processes work in conjunction with the [Product development flow](/handbook/product-development-flow/),
[Product designer workflow](https://about.gitlab.com/handbook/engineering/ux/ux-designer), and
[Engineering workflow](/handbook/engineering/workflow/), and the broader information about
contributing to GitLab found in [PROCESS.md](https://gitlab.com/gitlab-org/gitlab/blob/master/PROCESS.md).

## Documentation authoring and review

These processes are open to all GitLab contributors, so they are documented
within GitLab's documentation itself.

See the [Documentation process](https://docs.gitlab.com/ee/development/documentation/workflow.html) page.

## UI text

### Planning and authoring

A Product Designer should consult with the Technical Writer for their stage group when planning to add
or change substantial text within the UI, such as a phrase of explanatory microcopy or a link to documentation.

The technical writer can offer an initial review of any ideas, plans, or actual text, and can be asked to
draft text when provided with information on the context and goals of the text. Context may include
detail on the scenarios in which the text would appear (for example, to all users viewing the feature
or only under certain conditions), and the information to convey, which typically answers
one or more of the questions:

- What does this do?
- How do I use it?
- Why should I care?

### MR Reviews

Once the merge request is created, all changes and additions to text in the UI *must* be reviewed by the Technical Writer.
These may include labels (buttons, menus, column headers, UI sections) or any phrases that may be displayed within the UI,
such as user-assistance microcopy or error messages.

### Resources

Technical writers should familiarize themselves with these Pajamas Design System pages, use them when working with UI Copy, and contribute improvements to them if possible: 

- [Voice and Tone](https://design.gitlab.com/content/voice-tone)
- [Terminology](https://design.gitlab.com/content/terminology)
- [Punctuation](https://design.gitlab.com/content/punctuation), using them when working

Additional information about composing and reviewing UI text:

- "[Copy That: Helping your Users Succeed with Effective Product Copy](https://www.youtube.com/watch?v=M_Q1RO0ky2c)", a talk by Sarah Day.

## Release posts

The technical writer for each [stage group](https://about.gitlab.com/handbook/product/categories/) 
is a reviewer of their group's [feature blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-blocks) authored by the Product Manager.

For each release, a single technical writer is also assigned to the Release Post [Structural Check](https://about.gitlab.com/handbook/marketing/blog/release-posts/#structural-check).

## Workflow labels

Labels are described in the Development documentation [Issues workflow](https://gitlab.com/gitlab-org/gitlab/blob/master/doc/development/contributing/issue_workflow.md) page.
