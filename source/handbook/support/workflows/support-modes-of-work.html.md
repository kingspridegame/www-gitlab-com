---
layout: handbook-page-toc
title: Support Modes of Work (and how to do them)
category: Handling tickets
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Modes of Active Support Work

At GitLab, we want to create the support team of the future. Remote is a great first step, but one of the biggest challenges in any support organization is accountability. How do we maintain accountability while also encouraging flexibility? We currently have four rotating roles so that people know what to do and have variation in their work:

1. [On call](/handbook/support/on-call/)
1. [First Response Time Hawk](#first-response-time-hawk)
1. [SLA Hawk](#sla-hawk)
1. [My tickets](#my-tickets)



## First Response Time Hawk
### What is a First Response Time Hawk?

This is a rotating role, that will have someone be "on point" weekly, to make sure:

1. new Premium SLA tickets get a first response as soon as possible
1. all new tickets are appropriately categorized and prioritized as they arrive

First Response Time Hawks directly drive achievement in our [Service Level Agreement KPI](/handbook/support/#service-level-agreement-sla).

### The Process in Action

1. Every Week on Fridays, Managers will add a First Response Hawk for each region to the Support Week in Review to announce who will be responsible that week.
1. If you are FRT Hawk, you are responsible for:
    1. Premium and Ultimate first replies for tickets created from 9am - 5pm in your time zone
       - **Note:** if you are a US Citizen and have access to the Federal instance, this should also be a part of your rotation.
    1. Triaging tickets to make sure they have:
      * An associated organization
      * [The correct priority](/handbook/support/workflows/setting_ticket_priority.html#setting-ticket-priority)
      * The correct form (such as GitLab.com, if they're asking for GitLab.com support)
      * The correct **Self-managed Problem Type** selected
1. [Keep an eye on the FRTH View in ZenDesk](https://gitlab.zendesk.com/agent/filters/360060245433)
1. You have full authority to call in and ask others for help if volume is high, or you are stumped.
1. You should expect to see new and different things that you are not an expert in. Where possible, take the time to dive deep and try and understand the problem.

### Things to be mindful of

1. Given that there's a FRT Hawk _per timezone_, your shift will overlap with another hawk.
    * This means you have a teammate within a team; yay! Be sure to communicate with your fellow hawk so you can help each other out in case one of you is stuck on a ticket, and also to make sure you're not
    both quietly working on the same ticket

1. Your focus should be new tickets in the Premium & Ultimate queue, especially high and medium priority. If there are no new tickets in that queue you can keep jump to the Starter & Free queue.

1. You might not cover all the tickets you wanted to cover on your shift, and that's okay
   * Just do your best 
   * Take a break if you need one - making sure your team knows you're stepping away by posting in `#support-team-chat`
   * Ask the rest of the team for help when needed in `#support_self-managed`

### Dashboards

You can track your progress and see global results on [FRT Zendesk Dashboard](https://gitlab.zendesk.com/agent/reporting/analytics/period:0/dashboard:ab7DLgKtesWr)

## SLA Hawk
### What is the SLA Hawk role?
The SLA Hawk is a rotating role that you will carry out for one week at a time (Monday to Friday). 

The SLA Hawk is responsible for ensuring that action is taken on the 'most breached' and 'next to breach' tickets to in order to meet our SLAs and that difficult tickets are not ignored.

The SLA Hawk should also perform a queue tidy up:
* Ensure tickets are in correct queue
* Tickets have the correct Priority
* Tickets have the correct self-managed problem type
* The Customer has a Support Contract
* Solve the ticket if the support request is solved

SLA Hawks drive achievement via our KPI's for [Service Level Agreement KPI](/handbook/support/performance-indicators/#service-level-agreement-sla) and [Customer Satisfaction](/handbook/support/performance-indicators/#support-satisfaction-ssat) by making sure our customers are updated in a timely manner.

### The Process in Action

1. Each day, sort the Zendesk view for [Self Managed Premium and Ultimate](https://gitlab.zendesk.com/agent/filters/304899127) by '*Next SLA Breach*' ascending, starting with most breached ticket first.
2. Read the ticket yourself and if you feel you know what the next action is, do that (e.g. ask the customer for more information, solve the ticket if the issue is resolved, send your own reply if you have a great idea). There's no need to spend too long at this stage - 10 minutes at most. If the next action requires more work move on to the next step.
3. If you feel the priority does not match our [Definitions of Support Impact](/support/#definitions-of-support-impact) reach out to the customer and agree on the new priority.
4. If a ticket **has an assignee**:
   1. If the assignee is in your Region, link the ticket in `#support-self-managed` and ping them asking if they are able to take a look.
   2. If the assignee is from another Region and its during their “office hours” ask them if they could take a look. 
   3. If the assignee is offline and the ticket/customer needs an update, follow the **has no assignee** process below.
   4. If assignee can’t currently work ticket ask them to follow the Process in action for [My Tickets](https://about.gitlab.com/handbook/support/workflows/support-modes-of-work.html#the-process-in-action-2).
5. If a ticket **has no assignee**:
   1. Check to see if an engineer from your region has previously responded on the ticket, if so, ask them would they have the bandwidth to take the ticket.
   2. Let the team know (e.g. `@support-team-emea` ) about the ticket.
      - Once we’ve one or two volunteers, let the team know the ticket is taken care of via the thread.
   3. If you’re not getting any traction, and you feel you need help reach out to your manager.
   4. Once someone has taken ownership of the ticket, ask them to assign it to themselves.
6. If there are no imminent breaches in the 'Self Managed Premium and Ultimate' queue - repeat the process with the [Starter and Free](https://gitlab.zendesk.com/agent/filters/360062019453) view. 

### Team Responsibilities for the SLA Hawk
* Help the SLA Hawk! If they ask you for help, please acknowledge (You may not be able to help, just let them know). 
* Follow the [Support Modes of Work](https://about.gitlab.com/handbook/support/workflows/support-modes-of-work.html#the-process-in-action-2) for the tickets in your own queue (including finding another engineer to help or takeover the ticket).
* When looking for your next ticket, follow the [same process](/support/workflows/support-modes-of-work.html.md#the-process-in-action-1) as the SLA Hawk above (most breached ticket first).
* If you take a next response ticket and the ticket doesn’t have an assignee, assign yourself to the ticket.

## FAQ about SLA Hawk role

1. **Do we need to do the SLA Hawk role all day every day?** The role is not to work the tickets yourself but rather find an engineer to respond to the ticket. You probably only need to check for breached and breaching tickets 3 or 4 times per day, which fits in with our asynchronous working environment while ensuring that synchronous customer interaction is still maintained. This should make the role light weight and in between ticket checkins, you can continue with your planned day.
1. **Can we ask someone else to reply to a ticket?** Absolutely! If a team-mate is online and already assigned to a ticket or has context about it, please ask them in `#support_self-managed` if they can work on the ticket now and free you up to move on.

### Dashboards
Coming soon.

## My Tickets
### What is the "My Tickets" workflow?
If you don't have a specialized role this week, then you'll be working the "My Tickets" workflow. This is your 'normal' role - you're free to work on what makes most sense to you (including learning, writing up issues, improving docs etc).

The process here explains what to do when your actively working on tickets in Zendesk. You'll be moving issues along and acting as an escalation point for those in Hawk roles or Emergencies. Additionally, in this mode you'll be acting as a steward of longer running issues - making sure that a single person has the context necessary to drive things to resolution.

**By assigning a ticket to yourself, you become the current 'custodian' for the ticket. The whole team will still help and reply to all tickets, so you can expect others to step in and add replies. The reason for taking assignment is to help ensure tickets don't get ignored or stuck. If you have an assigned ticket that's stuck, follow the escalation process described below to get it moving along. You don't need to know how to solve the ticket because it's assigned to you.**

If you respond to a ticket and set it to `Open` or `On-hold`, ZenDesk will remove the SLA and automatically assign it to you (if it's not already assigned to someone else). If you don't have the expertise to work on the ticket, as usual, take stewardship and ask for help. You can ask an expert if they're willing to take the assignment if needed.

For all tickets where the user is waiting on a response or follow up from Support, always provide an update on its status and work that has been done. Ideally, once a day, but at least once every 3 days (the length of the On-hold period).

If you're working in "My Tickets", you're driving achievement in of our KPI of [Support Satisfaction](/handbook/support/#support-satisfaction-ssat).

### The Process in Action
1. At the start of the day, go to 'My assigned tickets' view and start at the top
1. Try to get a reply out for the top ticket.
1. If you don't know how to respond or can't come up with any additional actions, link the ticket in Slack and ask for help from your team-mates.
1. You can reassign a ticket to another colleague at any time
1. If you don't get any timely help from colleagues, `@mention` your manager in Slack in a thread where you initially linked the ticket and ask them what the next steps should be
1. Go to the next ticket down the list and repeat
1. If you run out of Assigned open tickets, follow the SLA Hawk workflow to find yourself your next ticket.

![My Tickets process flowchart](assets/my-tickets-workflow.png)

### Things to be mindful of
1. Teamwork is no less important with this workflow; be aware of what's happening across the board and jump in!
1. Assigning a ticket to yourself doesn't mean that you have to know how to solve it; it just means you have to keep the context of the ticket in your mind and help move things forward. If you're unable to do so, you can re-assign tickets to a colleague (with their permission, of course) or escalate to your manager by pinging them in Slack in `#support_self-managed` with a link to the ticket.

### Dashboards
Coming soon.

## Mermaid Source

#### SLA Hawk

~~~

   ```mermaid

   graph TD
   A[Sort 'Premium and Ultimate view by <br> 'Next SLA Breach'] -->
   B(Open the <br> top ticket)
   B --> C{Can you reply <br> to the ticket?}
   C -->|Yes| D[Send <br> reply] --> H
   C -->|No| E[Ask for help in Slack <br> and link the ticket]
   E -->|Help received| F[Send <br> reply] --> H
   E -->|No help recieved| G[Mention your manager in Slack <br> on the ticket thread] --> H[Go to the next ticket <br> - if no imminent breaches - <br> go to 'Starter and Free' view] --> B
   ```

~~~

#### My Tickets

~~~

   ```mermaid

   graph TD
   A[Open the 'My Assigned Tickets View'] -->
   B(Open the top ticket)
   B --> C{Can you reply to the ticket?}
   C -->|Yes| D[Send reply] --> H
   C -->|No| E[Ask for help in Slack and link the ticket]
   E -->|Help received| F[Send reply] --> H
   E -->|No help recieved| G[Mention your manager in Slack on the ticket thread] --> H[Any tickets left in 'My Assigned Tickets' view?]
   H --> |Yes| I[Repeat this process] --> B
   H --> |No| J[Open the main views and find a ticket to work on]
   --> K(Send a reply and assign to yourself) --> J

   ```
~~~
